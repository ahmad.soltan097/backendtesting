const { query,validationResult } = require('express-validator');
const knex = require("knex")({
    client: "mysql",
    connection: {
        host: process.env.dbhost,
        port: process.env.dbport,
        user: process.env.dbuser,
        password: process.env.dbpassword,
        database: process.env.dbname,
        typeCast: function castField(field, useDefaultTypeCasting) {
            if ((field.type === "BIT") && (field.length === 1)) {
                var bytes = field.buffer();
                return bytes ? bytes[0] == 1 : null;
            }
            return (useDefaultTypeCasting());
        }
    }
});

console.log("testAdnan");

exports.testPerformance = [
    async (req, res, next) => {
        try {
            const data = await Promise.all(
                (await knex('companies').select('id', 'name')).map(async company => {
                    company.users = await knex('users').where('company_id', company.id).select('email', 'id', 'phone');
                    return company;
                })
            );
            res.status(200).json(data);
        } catch (err) {
            next(err);
        }
    }
]
//Expression #1 
// of SELECT list is not in GROUP BY clause 
// and contains nonaggregated column 'returntr_prod.tbl_customer_pod_uploads.id' 
// which is not functionally dependent on columns in GROUP BY clause; 
// this is incompatible with sql_mode=only_full_group_by
//Solve :
// preform this in ur mysql server "SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));"
exports.testPerformance2 = [
    async (req, res, next) => {
        try {
            const data = await Promise.all(
                (await knex('companies')
                    .select('companies.id as companiyId',
                        'companies.name',
                        'email',
                        'users.id as userId',
                        'phone')
                    .join('users', 'company_id', 'companies.id')
                )
            );
            // the following lines is required to keep data in the same schema
            // but i try to make it in one loop 
            const companies = {}
            const reuslt = []
            data.forEach(v => {
                var index = companies[v.companiyId]

                if (index == undefined)
                    companies[v.companiyId] = index = reuslt.push({
                        id: v.companiyId,
                        name: v.name,
                        users: []
                    });

                const { userId: id, email, phone } = v
                reuslt[index - 1].users.push({ id, email, phone })

            })
            res.status(200).json(reuslt);
        } catch (err) {
            next(err);
        }
    }
]

exports.measurements = [
    //validate ids 
    query('ids').exists().isString().matches(/^\d+(,\d+)*$/),
    async (req, res, next) => {
        try {
            const result = validationResult(req);
            if (!result.isEmpty()) {
                return res.status(400).json({ errors: result.array() });
            }
            const ids = req.query.ids.split(',').map(v=>Number(v))
            const q1 = knex('iot_measurements')
                .select('*')
                .whereIn('device_id',ids).as('A')
            //Obj 1
            const latestReading = await knex(q1)
                .orderBy('created_at', 'desc')
                .groupBy('device_id', 'sensor_id')

            const q2 = knex(q1)
                .distinct('device_id', 'sensor_id', 'status').as('B')
            //Obj 2
            const readingTimes = await knex(q2)
                .count('status')
                .groupBy('device_id', 'sensor_id')

            res.status(200).json({ latestReading, readingTimes });
        } catch (err) {
            next(err);
        }
    }
]
